# Codeberg Moderation tools

This is an attempt to write a frontend that makes moderating Codeberg or other Gitea instances easier.
Some functionality here is probably duplicated to the native Gitea tooling, but
we think it's worth it.

This is not going to improve the Gitea moderation tools (although necessary):

- focus on batch operations and might be overkill to small instances
- runs as independent service and does not bloat Gitea codebase
- includes some codeberg-specific stuff, like license reminders, some custom abuse messages etc

## Design

The idea is pretty simple: You put a list of content you want to process, e.g. a list of repos.
You have a handful actions you can take on this, e.g. expand to the forks, or reduce only to the users of them.
Each plugin produces an output list, or multiple, and you can `TAKE` them back as your next input.
Finally, you can act on the list, e.g. remove or lock, or quarantine, or send them an email.

We now decided to handle much of this in the backend, this frontend then calls this backend, that in term calls the Gitea API.

We want to  build the service with as less need for server-side configuration, especially security tokens, as possible.
Everything should be handled via client-side API tokens (or OAuth).
The configuration of access levels should be handled with a Gitea org and teams in there.

## Roadmap

I consider some features with about this priority:

- [ ] remove spam users *including* their repos and orgs (currently not possible with Gitea, and annoying)
- [ ] allow to quarantine repos more easily
- [ ] search for (spam) users and repos to more easily process them
- [ ] delete issues completely
- [ ] write a backend that allows to send emails
- [ ] add some more features, e.g. a "Keep or Sweep" mode to quickly filter for unwanted content
- [ ] maybe add OAuth so the login is easier
- [ ] after OAuth: allow users to report spam to this system, if Gitea still doesn't have a way to report content
- [ ] maybe allow to grant access to more moderators (they could propose changes that are stored in an internal database and confirmed by admins)

## Contributing

The Codeberg team is very open to your contribution. Since we are working nicely in a team, and this is a young project in the bootstrapping phase, it might be hard at times to get started (still check out the issues, we always aim to have some things to get you started).

If you have any questions, want to work on a feature or could imagine collaborating with us for some time, feel free to ping us in an issue or in a general Matrix chatgroup.

This project is maintained by:

- [ashimokawa](https://codeberg.org/ashimokawa)
- [fnetX](https://codeberg.org/fnetX) - contact me via E-Mail, Matrix, XMPP (links in profile)

The system consists of the backend that handles the API queries from the frontend and maps them to Gitea API queries.
Most logic, especially if it requires larger data transfers between the backend and the Gitea API, should be done on the server.
The user interface should be as lightweight as possible, so moderators can also do operations on mobile and don't need to carry a powreful machine with them.

The backend uses go-chi and the Gitea SDK to be compatible with the Gitea ecosystem. The frontend uses plain Javascript as of today, read @fnetx personal opinion below:

## WTF what have you done to this SPA?

I know my way of building this single page application is far from the industry standard, but I'm very annoyed by all those
Javascript frameworks and their dependency management and the whole ecosystem. I would have preferred to write this without JavaScript and its ecosystem at all, and tried to work around npm, but had to add it in the end.

Well, I spent two nights setting up some build environment, and from my experience, getting started with javascript frameworks isn't always straightforward either, but now I wanted to finally get my hands on and make this. My motivation to do this is because I need the tooling, not because I'm too bored. And doing some plain javascript works somewhat faster for me than playing around with yet-another-JS-framework. Look, if I started writing the HTTP API queries by hand, I would have had more progress by now than the very first API request.

Another thing I considered is the maintenance overhead of hobby-JS projects. From my experience, it's hardly possible to leave them alone for a month and continuing where you left of. Instead, you'll usually have to update some dependencies and deal with breakages. I'm not saying that it's always easier to maintain a plain JS project, I don't think so when you are working with this stuff daily. But when you have some hobby projects you only look at from time to time, Javascript always keeps a surprise for you. With some of my other projects, I sometimes spent all the free time I had for a project in updating its dependencies, without having the time to do actual codework. I wanted to try out if this is indeed better without all this hazzle, so I started a close-to-plain-JS project again.

If you think this project needs to be done more serious, I'll gladly hand over the maintenance or accept contributions that increase maintainablity or speed up development. But as long as I'm the only one working on it, I'll try to avoid the tooling that creates headache for me.

Thank you for understanding :-)
