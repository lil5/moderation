module moderation

go 1.16

require code.gitea.io/sdk/gitea v0.15.1

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/docgen v1.2.0 // indirect
	github.com/go-chi/jwtauth/v5 v5.0.2
	github.com/go-chi/render v1.0.1
	github.com/go-ini/ini v1.66.3
	github.com/hashicorp/go-version v1.2.1 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
)
