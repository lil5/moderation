package main

import (
	"bytes"
	"fmt"

	"github.com/go-ini/ini"
	"gopkg.in/gomail.v2"
	"text/template"
)

var mailDialer *gomail.Dialer
var mailFromname string
var mailAlwaysBcc string

func mailConfig(cfg *ini.File) {
	mailconfig := cfg.Section("mail")
	hostname := mailconfig.Key("HOSTNAME").String()
	port := mailconfig.Key("PORT").MustInt()
	username := mailconfig.Key("USERNAME").String()
	password := mailconfig.Key("PASSWORD").String()
	mailFromname = mailconfig.Key("FROMNAME").String()
	mailAlwaysBcc = mailconfig.Key("BCC").String()
	mailDialer = gomail.NewDialer(hostname, port, username, password)
}

func mailSend(toMail string, toName string, templateName string, templateData map[string]interface{}) {
	mailTemplate, err := template.ParseFiles("templates/mail/" + templateName + ".tmpl")
	if err != nil {
		fmt.Printf("Error reading mail template: %s", err.Error())
	}

	templateData["giteaURL"] = giteaURL
	templateData["toName"] = toName
	var mailContent bytes.Buffer
	if err = mailTemplate.Execute(&mailContent, templateData); err != nil {
		fmt.Printf("Error parsing mail template: %s", err.Error())
	}

	mail := gomail.NewMessage()
	mail.SetHeader("From", mailFromname)
	mail.SetHeader("To", toMail)
	if mailAlwaysBcc != "" {
		mail.SetHeader("Bcc", mailAlwaysBcc)
	}
	mail.SetHeader("Subject", "Important notice regarding your content on Codeberg.org")
	mail.SetBody("text/plain", mailContent.String())
	if err = mailDialer.DialAndSend(mail); err != nil {
		fmt.Printf("Error sending email: %s", err.Error())
	}
}
