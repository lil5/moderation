package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"code.gitea.io/sdk/gitea"

	"github.com/go-chi/chi/v5"
	"github.com/go-ini/ini"
)

var reposQuarantineNewOwner string

type RepoData struct {
	Owner string `json:"owner"`
	Name  string `json:"name"`
}

type RepoRequest struct {
	GenericRequest
	List []RepoData `json:"list"`
}

func repoRoutes(r chi.Router) {
	r.Post("/quarantine", repoQuarantine)
	r.Post("/inform", repoInformOwner)
}

func repoConfig(cfg *ini.File) {
	repoconfig := cfg.Section("repos")
	reposQuarantineNewOwner = repoconfig.Key("QUARANTINE_OWNER").String()
}

func repoTransfer(currentOwner string, currentName string, newOwner string, newName string) error {
	if currentOwner == "" || currentName == "" || newOwner == "" || newName == "" {
		return &GenericError{"repoTransfer", "some empty value passed"}
	}
	var tro gitea.TransferRepoOption
	tro.NewOwner = newOwner

	_, res, err := giteaClient.TransferRepo(currentOwner, currentName, tro)
	if err != nil {
		return &GenericError{"Transfer Repo: ", "res.Status"}
	}

	var ero gitea.EditRepoOption
	ero.Name = &newName
	_, res, err = giteaClient.EditRepo(newOwner, currentName, ero)
	if err != nil {
		return &GenericError{"Change Repo Name: ", res.Status}
	}
	return nil
}

func repoQuarantine(w http.ResponseWriter, r *http.Request) {
	request := RepoRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		fmt.Printf("Error decoding JSON: %s\n", err.Error())
		return
	}

	for _, repodata := range request.List {
		giteaUser, res, err := giteaClient.GetUserInfo(repodata.Owner)
		if err != nil {
			fmt.Println("Get User Info: " + res.Status)
		}

		newName := strconv.FormatInt(time.Now().Unix(), 10) + "_" + strconv.FormatInt(giteaUser.ID, 10) + "_" + repodata.Owner + "_" + repodata.Name
		err = repoTransfer(repodata.Owner, repodata.Name, reposQuarantineNewOwner, newName)

		if err != nil {
			w.Write([]byte(err.Error() + ","))
		}

		mailTemplateData := make(map[string]interface{})
		mailTemplateData["repoName"] = repodata.Name
		if request.InformUser {
			mailTemplateData["reason"] = request.InformUserAdditionalReason
		}
		mailSend(giteaUser.Email, repodata.Owner, "quarantine", mailTemplateData)
	}
}

func repoInformOwner(w http.ResponseWriter, r *http.Request) {
	request := RepoRequest{}
	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		fmt.Printf("Error decoding JSON: %s\n", err.Error())
		return
	}

	for _, repodata := range request.List {
		giteaUser, res, err := giteaClient.GetUserInfo(repodata.Owner)
		if err != nil {
			fmt.Println("Get User Info: " + res.Status)
		}

		mailTemplateData := make(map[string]interface{})
		mailTemplateData["repoName"] = repodata.Name
		if request.InformUser {
			mailTemplateData["reason"] = request.InformUserAdditionalReason
		}
		mailSend(giteaUser.Email, repodata.Owner, "quarantine", mailTemplateData)
	}
}
